class Status {
  String name;
  String statusDate;

  Status({
    required this.name,
    required this.statusDate,
  });
}

var statusList = [
  Status(name: "Jane", statusDate: "Today, 12:10 PM"),
  Status(name: "Joni", statusDate: "Today, 10:47 PM"),
  Status(name: "Jono", statusDate: "Today, 09:30 PM"),
];
