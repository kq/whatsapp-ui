class Call {
  String name;
  String callDate;

  Call({
    required this.name,
    required this.callDate,
  });
}

var callList = [
  Call(name: "Jono", callDate: "Yesterday, 12:10 PM"),
  Call(name: "Jane", callDate: "Yesterday, 10:47 PM"),
  Call(name: "Joni", callDate: "Yesterday, 09:30 PM"),
];
