import 'package:flutter/material.dart';
import 'package:whatsapp_ui/models/call.dart';
import 'package:whatsapp_ui/theme.dart';

class CallView extends StatelessWidget {
  const CallView({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: callList.length,
      itemBuilder: (context, index) {
        final call = callList[index];
        return ListTile(
          leading: const Icon(
            Icons.account_circle,
            color: Colors.black45,
            size: 58.0,
          ),
          title: Text(
            call.name,
            style: customTextStyle,
          ),
          subtitle: Text(
            call.callDate,
            style: const TextStyle(
              color: Colors.black45,
              fontSize: 16,
            ),
          ),
          trailing: const Padding(
            padding: EdgeInsets.only(bottom: 5.0),
            child: Icon(
              Icons.call,
              color: Color.fromRGBO(18, 140, 126, 1.0),
            ),
          ),
        );
      },
    );
  }
}
